 System vimrc file for MacVim
"
" Maintainer:	Bjorn Winckler <bjorn.winckler@gmail.com>
" Last Change:	Sat Aug 29 2009

set nocompatible

" The default for 'backspace' is very confusing to new users, so change it to a
" more sensible value.  Add "set backspace&" to your ~/.vimrc to reset it.
set backspace+=indent,eol,start

" Disable localized menus for now since only some items are translated (e.g.
" the entire MacVim menu is set up in a nib file which currently only is
" translated to English).
set langmenu=none

filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

 " let Vundle manage Vundle
 " required! 
 Bundle 'gmarik/vundle'

 " My Bundles here:
 "
 " original repos on github

"" Sparkup is like emmet expansion
 Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}

"" Not sure what easy motion does - find out
 Bundle 'Lokaltog/vim-easymotion'

"" Autocomplete and more
 Bundle 'Valloric/YouCompleteMe'

"" My awesome snippets plugin
Bundle 'SirVer/ultisnips'

"" This is the NERDTree!
 Bundle 'scrooloose/nerdtree'

"" Adds a little functionality to NERDTree
 Bundle 'jistr/vim-nerdtree-tabs'

"" Not sure what this is either??
 Bundle 'tpope/vim-fugitive'

" Manage Sessions with :SaveSession [name] and :OpenSession [name]
Bundle 'xolox/vim-session'

" XDebug Support
Bundle 'joonty/vdebug'

" Not sure if this will install
"Bundle 'michalliu/jsruntime.vim'

"" Bundle 'tpope/vim-rails.git'
"" Bundle "MarcWeber/vim-addon-mw-utils"
"" Bundle "tomtom/tlib_vim"
"" Bundle "garbas/vim-snipmate"




 " vim-scripts repos

"" Set snipmate custom file
"" let g:snipMate = {}
"" let g:snipMate.scope_aliases = {}
"" let g:snipMate.scope_aliases['sql'] = 'sql'

cmap w!! %!sudo tee %

set number
set relativenumber

"" imap zz <esc>a<Plug>snipMateNextOrTrigger
"" smap zz <Plug>snipMateNextOrTrigger

nnoremap <Leader>e :e <C-R>=expand('%:p:h') . '/'<CR>
nnoremap <Leader>s :SaveSession! 
nnoremap <Leader>o :OpenSession! 
nnoremap <Leader>r :RestartVim 
nnoremap <Leader>b :Bookmark
nnoremap <Leader>c :colorscheme
nnoremap <Leader>f :NERDTreeFind

" Autocomplete - and = to -> and =>
"inoremap <expr> <Tab> getline('.')[col('.')-2]=~#'[-=]' ? ">" : "\<Tab>"
inoremap <expr> - getline('.')[col('.')-2]=~#'[-=]' ? ">" : "-"

"" YouCompleteMe
"" let g:ycm_key_list_previous_completion=['<Up>']

"" Ultisnips
"" let g:UltiSnipsExpandTrigger="`"
"" let g:UltiSnipsListSnippets="`"



"" NERDTree
autocmd vimenter * if !argc() | NERDTree | endif
map <C-n> :NERDTreeToggle<CR>
map <C-f> :NERDTreeFind<CR>
"" Close nerdtree if last open window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
map zzz <C-W>w



" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

set noswapfile

" Ultisnips and Youcompleteme play nice
function! g:UltiSnips_Complete()
    call UltiSnips_ExpandSnippet()
    if g:ulti_expand_res == 0
        if pumvisible()
            return "\<C-n>"
        else
            call UltiSnips_JumpForwards()
            if g:ulti_jump_forwards_res == 0
               return "\<TAB>"
            endif
        endif
    endif
    return ""
endfunction

au BufEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<cr>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsListSnippets="<c-e>"


" Manage window movement - swap window placements  """"""""""""""""""""""""""""""""""
" Type \mw in first window - \pw in second window :  mark window - paste window
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    "Mark destination
    let curNum = winnr()
    let curBuf = bufnr( "%" )
    exe g:markedWinNum . "wincmd w"
    "Switch to source and shuffle dest->source
    let markedBuf = bufnr( "%" )
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' curBuf
    "Switch to dest and shuffle source->dest
    exe curNum . "wincmd w"
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf 
endfunction

nmap <silent> <leader>mw :call MarkWindowSwap()<CR>
nmap <silent> <leader>pw :call DoWindowSwap()<CR>

set splitbelow
set splitright

"""""""""""""""""""""""""""
syntax on

let g:session_autosave = 'no'

let g:vdebug_options = {
 'server': '192.168.28.215'
}

colorscheme desert
let NERDTreeShowBookmarks=1

"##############################################################################                                                                         
" Easier split navigation                                                                                                                               
"##############################################################################                                                                         

" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>                                                                                                                       
nmap <silent> <c-j> :wincmd j<CR>                                                                                                                       
nmap <silent> <c-h> :wincmd h<CR>                                                                                                                       
nmap <silent> <c-l> :wincmd l<CR>
